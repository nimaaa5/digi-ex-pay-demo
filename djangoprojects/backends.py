from django.contrib.auth.backends import ModelBackend
from exchane.models import Account


class CustomerBackend(ModelBackend):

    def authenticate(self, request, **kwargs):
        customer_id = kwargs['username']
        password = kwargs['password']
        try:
            customer = CustomerUser.objects.get(customer_id=customer_id)
            if customer.user.check_password(password) is True:
                return customer.user
            else:
                return None
        except Customer.DoesNotExist:
            return None
