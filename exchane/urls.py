from django.urls import path
from . import views

urlpatterns = [
    path('', views.home , name='home'),
    path('hd', views.homeTable , name='homeTable'),
    path('h',views.index,name="home1"),
    path('register',views.register,name="register"),

    path('index', views.index, name='index'),
    path('wallet', views.wallet, name='wallet'),
    path('transactions', views.transactions, name='transactions'),
    path('deposit_irr', views.deposit_irr, name='deposit_irr'),
    path('withdraw_irr', views.withdraw_irr, name='withdraw_irr'),
    path('deposit_crypto', views.deposit_crypto, name='deposit_crypto'),
    path('buy_crypto', views.buy_crypto, name='buy_crypto'),
    path('get_transactions', views.get_transactions, name='get_transactions'),
    path('get_assets', views.get_assets, name='get_assets'),
    path('complete_account', views.complete_account, name='complete_account'),
    path('test/<str:s>', views.test, name='test'),
    path('getWallets/<str:coin>', views.getWallets, name='getWallets'),
    path('Withdraw/<str:coin>', views.Withdraw, name='Withdraw'),
    path('deposit/<str:coin>', views.deposit, name='deposit'),
    path('getCoins', views.getCoins, name='getCoins'),
    path('get_tether_price', views.get_tether_price, name='get_tether_price'),
    path('withdraw_crypto', views.withdraw_crypto, name='withdraw_crypto'),
    path('deposit_history', views.deposit_history, name='deposit_history'),
    path('exchange', views.exchange, name='exchange'),
    path('logout', views.logout, name='logout'),
    path('userCard', views.userCard, name='userCard'),
    path('veriftyPhoneNumber', views.veriftyPhoneNumber, name='veriftyPhoneNumber'),
    path('register', views.register, name='register'),
    path('verification', views.verification, name='verification'),

]
