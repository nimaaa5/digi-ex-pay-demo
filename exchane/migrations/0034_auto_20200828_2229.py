# Generated by Django 3.0.7 on 2020-08-28 17:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('exchane', '0033_remove_account_email'),
    ]

    operations = [
        migrations.AddField(
            model_name='account',
            name='otp',
            field=models.CharField(max_length=5, null=True),
        ),
        migrations.AddField(
            model_name='account',
            name='otpCreatedAt',
            field=models.DateField(auto_now_add=True, null=True),
        ),
        migrations.AddField(
            model_name='account',
            name='phoneIsVerified',
            field=models.BooleanField(default=False, null=True),
        ),
    ]
