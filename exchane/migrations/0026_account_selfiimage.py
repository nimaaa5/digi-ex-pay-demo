# Generated by Django 3.0.7 on 2020-08-24 21:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('exchane', '0025_auto_20200825_0126'),
    ]

    operations = [
        migrations.AddField(
            model_name='account',
            name='selfiImage',
            field=models.ImageField(default='pic_folder/None/no-img.jpg', upload_to='pic_folder/'),
        ),
    ]
