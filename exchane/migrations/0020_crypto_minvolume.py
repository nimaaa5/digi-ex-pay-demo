# Generated by Django 3.0.7 on 2020-08-06 21:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('exchane', '0019_account_isverified'),
    ]

    operations = [
        migrations.AddField(
            model_name='crypto',
            name='minVolume',
            field=models.FloatField(default=0),
        ),
    ]
