# Generated by Django 3.0.7 on 2020-07-20 21:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('exchane', '0013_delete_tb_tutorial_collection'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='statusb',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='transaction',
            name='timeb',
            field=models.CharField(default='', max_length=100),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='transaction',
            name='wallet_idb',
            field=models.CharField(max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='cryptosell',
            name='txId',
            field=models.CharField(max_length=100, unique=True),
        ),
    ]
