import os
import logging
from telegram.ext import MessageHandler, Filters
from binance.client import Client
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
import json
import requests
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from django.utils import timezone
import datetime
import json
from collections import namedtuple
from django.shortcuts import redirect
from random import randint

from django.contrib.auth.models import User
from django.contrib.auth.forms import UserChangeForm, PasswordChangeForm
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required

from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from .models import UserCard, TransAction, UserAssets, TTRPrice, vandarRequests, Account, Crypto, CryptoWallet, Exchange
from .forms import UserCardForm, Verification, ExchangeForm, RialDepositForm, CryptoSellForm, CryptoBuyForm, RialWithdrawForm, CompleteAccountForm, CryptoWithdraw, RegistrationForm

# /////bot//////////
from telegram.ext import Updater
from telegram.ext import CommandHandler

# /////end bot ////////////

from django.contrib.auth import get_user_model
User = get_user_model()

client = Client("5YXvlrWklHu7382z6pc0m8MK8uGys9IrqUcMmQuF1dKEqXZSgQjXWiYomGffpV3Z",
                "r6BxlOiYqPUdPtXJPnZl1cTX4YXYeLY7SFihmUC4ENfxB8cRNOFtywktXNrECu3q")
# Create your views here.

# //////////////bot///////////////////


# updater = Updater(
#     token='1318680080:AAHjsioEGV-SGX8NJQ7B71lg2lqDGoFLTE0', use_context=True)
# dispatcher = updater.dispatcher
# logging.basicConfig(
#     format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)


# def start(update, context):
#     context.bot.send_message(
#         chat_id=update.effective_chat.id, text="I'm a bot, please talk to me!")


# def trans0(update, context):
#     if update.effective_chat.id == 93520350:
#         jData = TransAction.objects.filter(status=0).values()
#         for x in jData:
#             context.bot.send_message(
#                 chat_id=update.effective_chat.id, text=json.dumps(x, default=default).encode("utf-8"))
#     else:
#         context.bot.send_message(chat_id=update.effective_chat.id, text="--")


# def trans(update, context):
#     if update.effective_chat.id == 93520350:
#         id = int(update.message.text.replace("/trans ", ""))
#         jData = TransAction.objects.filter(id=id).values()
#         for x in jData:
#             context.bot.send_message(
#                 chat_id=update.effective_chat.id, text=json.dumps(x, default=default).encode("utf-8"))
#     else:
#         context.bot.send_message(chat_id=update.effective_chat.id, text="--")


# def user(update, context):
#     if update.effective_chat.id == 93520350:
#         id = int(update.message.text.replace("/user ", ""))
#         jData = Account.objects.filter(id=id).values()
#         for x in jData:
#             context.bot.send_message(
#                 chat_id=update.effective_chat.id, text=json.dumps(x, default=default).encode("utf-8"))
#     else:
#         context.bot.send_message(chat_id=update.effective_chat.id, text="--")


# start_handler = CommandHandler('start', start)
# trans0_handler = CommandHandler('trans0', trans0)
# trans_handler = CommandHandler('trans', trans)
# user_handler = CommandHandler('user', user)
# dispatcher.add_handler(start_handler)
# dispatcher.add_handler(trans0_handler)
# dispatcher.add_handler(trans_handler)
# dispatcher.add_handler(user_handler)
# updater.start_polling()


# def echo(update, context):
#     context.bot.send_message(chat_id=update.effective_chat.id, text="echo--")


# echo_handler = MessageHandler(Filters.text & (~Filters.command), echo)
# dispatcher.add_handler(echo_handler)


# def caps(update, context):
#     text_caps = ' '.join(context.args).upper()
#     context.bot.send_message(chat_id=update.effective_chat.id, text=text_caps)


# caps_handler = CommandHandler('caps', caps)
# dispatcher.add_handler(caps_handler)

# ////////////end bot/////////////////


def _json_object_hook(d):
    return namedtuple('X', d.keys())(*d.values())


def json2obj(data):
    return json.loads(data, object_hook=_json_object_hook)


def get_tether_price(request):
    ttrPrice = TTRPrice.objects.latest('id')
    return JsonResponse({'sell_price': ttrPrice.sell_price, 'buy_price': ttrPrice.buy_price}, safe=False)


def deposit_history(request):
    deposits = client.get_deposit_history().values()
    dep = list(deposits)
    dep1 = dep[1]
    dep2 = dep1[0]
    return JsonResponse(dep2, safe=False)


def test(request, s):
    ttrPrice = TTRPrice.objects.latest('id')
    if s.upper() == "USDT":
        return JsonResponse(ttrPrice.buy_price, safe=False)

    jData = client.get_all_tickers()
    data = list(jData)
    mdata = data[0]
    for item in data:
        if item["symbol"] == s.upper()+"USDT":
            mdata = item

    res = float(mdata["price"]) * ttrPrice.buy_price
    return JsonResponse(res, safe=False)


@login_required
def complete_account(request):
    if request.method == "POST":
        form = CompleteAccountForm(request.POST)
    if form.is_valid():
        post = form.save(commit=False)
        request.user.shabaNumber = post.shabaNumber
        request.user.accountName = post.accountName
        request.user.nationalCode = post.nationalCode
        request.user.save()

    return render(request, "index.html", {'name': request.user.username, 'total': request.user.totalRial, 'block_': request.user.blockedRial})


@login_required
def get_assets(request):
    jData = UserAssets.objects.filter(author=request.user).values()
    return JsonResponse({'data': list(jData)}, safe=False)


@login_required
def get_transactions(request):
    jData = TransAction.objects.filter(author=request.user).values()
    return JsonResponse({'data': list(jData)}, safe=False)


@login_required
def deposit_irr(request):

    if request.method == "GET":
        return render(request, "index.html", {'total': request.user.totalRial, 'block_': request.user.blockedRial})

    if request.method == "POST":
        form = RialDepositForm(request.POST)
    if form.is_valid():
        post = form.save(commit=False)
        post.author = request.user
        post.date = timezone.now()
        post.save()
        message = ""
        token = ""

        # /////////// save Transaction //////////////

        trans = TransAction()
        trans.amount = post.amount
        trans.author = post.author
        trans.symbol = "irr"
        trans.order_date = post.date
        trans.last_action_date = timezone.now()
        trans.status = 0
        trans.wallet_id = post.author.shabaNumber
        trans.accountName = post.author.accountName
        trans.description = "در انتظار بررسی"
        trans.side = 1
        trans.save()

        # /////////// end save Transaction //////////////
        payload = {"api_key": "b44fbd8273bd9988900a64c7b969593b5d004046", "amount": post.amount * 10,  # rial -> toman
                   "callback_url": "http://localhost:8000/vandarResponsee", "mobile_number": "09010506336", "factorNumber": "12345", "description": "description"}

        r = requests.post('https://vandar.io/api/ipg/send', data=json.dumps(payload))
        v = json2obj(r.content)
        if v.status == 0:
            message = v.errors
        if v.status == 1:
            token = v.token

        vandar = vandarRequests()
        vandar.amount = post.amount
        vandar.token = token
        vandar.error = message
        vandar.transAction = trans
        vandar.status = v.status
        vandar.save()

    return redirect('https://vandar.io/ipg/'+v.token)


@login_required
def wallet(request):
    return render(request, "wallet.html", {'name': request.user.username, 'total': request.user.totalRial, 'block_': request.user.blockedRial, "available": request.user.availableRial})


@login_required
def transactions(request):
    return render(request, "transactions.html", {'name': request.user.username, 'total': request.user.totalRial, 'block_': request.user.blockedRial, "available": request.user.availableRial})


@login_required
def vandarResponsee(request, token):
    error = ""
    message = ""
    amount = 0
    fee = 0
    vandar = vandarRequests.objects.filter(token=token).values()
    if vandar != None & vandar.resstatus == None:
        payload = {
            "api_key": "b44fbd8273bd9988900a64c7b969593b5d004046", "token": token}
        r = requests.post('https://vandar.io/api/ipg/verify', data=json.dumps(payload))
        v = json2obj(r.content)
        vandar.resstatus = v.status
        if v.satatus == 1:
            vandar.resamount = v.amount
            vandar.resrealAmount = v.realAmount
            vandar.reswage = v.wage
            vandar.restransId = v.transId
            vandar.resfactorNumber = v.factorNumber
            vandar.resmobile = v.mobile
            vandar.resdescription = v.description
            vandar.rescardNumber = v.cardNumber
            vandar.respaymentDate = v.paymentDate
            vandar.rescid = v.cid
            vandar.resmessage = v.message
            message = v.message
            fee = v.amount - v.realAmount
            amount = v.realAmount
            request.user.totalRial += v.amount
            request.user.availableRial += v.amount
            request.user.save()

        data = UserCard.objects.filter(author=request.user)
        find = False
        for x in data:
            if x.cardNumber.endswith(v.cardNumber[len(v.cardNumber)-4:]):
                find = True

        if find != True:
            model = UserCard()
            model.cardNumber = v.cardNumber
            model.date = timezone.now()
            model.author = request.user
            model.verifiedLevel = -1
            model.save()
            request.user.unverifiedCardCount = request.user.unverifiedCardCount + 1
            request.user.save()

        if v.satatus == 0:
            vandar.reserrors = v.errors
            error = v.errors
    return render(request, "paymentResult.html", {'error': error, 'message': message, 'amount': amount, 'fee': fee})


@login_required
def withdraw_irr(request):
    if request.method == "GET":
        return render(request, "index.html", {'name': request.user.username, 'total': request.user.totalRial, 'block_': request.user.blockedRial, 'available': request.user.availableRial})
    message = ""
    if request.method == "POST":
        form = RialWithdrawForm(request.POST)
    if form.is_valid():
        post = form.save(commit=False)

        if request.user.availableRial >= post.amount:
            request.user.blockedRial += post.amount
            request.user.availableRial -= post.amount
            request.user.save()
            post.author = request.user
            post.date = timezone.now()
            post.save()
            # /////////// save Transaction //////////////
            trans = TransAction()
            trans.amount = post.amount
            trans.author = post.author
            trans.symbol = "irr"
            trans.order_date = post.date
            trans.last_action_date = timezone.now()
            trans.status = 0
            trans.wallet_id = post.author.shabaNumber
            trans.accountName = post.author.accountName
            trans.description = "در انتظار بررسی"
            trans.side = -1
            trans.save()
        else:
            message = "insufficient balance"

        # /////////// end save Transaction //////////////

    return render(request, "index.html", {'message': message, 'name': request.user.username, 'total': request.user.totalRial, 'block_': request.user.blockedRial, 'available': request.user.availableRial})


@login_required
def deposit_crypto(request):

    cryptosells = TransAction.objects.filter(amount=0).filter(side=-1)
    for sell in cryptosells:
        trans = list(json2obj(client.get_deposit_history().values()[1]))
        for tran in trans:
            if sell.txid == tran['txId']:
                sell.txid = tran['txId']
                sell.amount = tran['amount']
                sell.symbol = tran['asset']
                sell.statusb = tran['status']
                sell.timeb = tran['insertTime']
                sell.wallet_idb = tran['address']
                sell.save()
                if sell.statusb == 1:
                    uassets = UserAssets.objects.filter(
                        symbol=sell.symbol).values()
                    if uassets.count > 0:
                        uasset = uassets[0]
                        uasset.amount += sell.amount
                        uasset.save()
                    else:
                        uasset = UserAssets()
                        uasset.amount = sell.amount
                        uasset.save()

    if request.method == "POST":
        form = CryptoSellForm(request.POST)
    if form.is_valid():
        post = form.save(commit=False)
        post.author = request.user
        post.date = timezone.now()
        post.save()

        # /////////// save Transaction //////////////

        trans = TransAction()
        trans.amount = 0
        trans.author = post.author
        trans.symbol = "واریز رمز ارز"
        trans.order_date = post.date
        trans.last_action_date = timezone.now()
        trans.status = 0
        trans.txid = post.txId
        trans.accountName = post.author.accountName
        trans.description = "در انتظار بررسی"
        trans.side = -1
        trans.save()

        # /////////// end save Transaction //////////////
        #

    return render(request, "index.html", {'name': request.user.username, 'total': request.user.totalRial, 'block_': request.user.blockedRial})


@login_required
def buy_crypto(request):
    frm: any
    if request.method == "POST":
        form = CryptoBuyForm(request.POST)
        frm = form
    if form.is_valid():
        post = form.save(commit=False)
        post.author = request.user
        post.date = timezone.now()
        post.save()

        # /////////// save Transaction //////////////

        trans = TransAction()
        trans.amount = post.amount
        trans.author = post.author
        trans.symbol = post.symbol
        trans.order_date = post.date
        trans.last_action_date = timezone.now()
        trans.status = 0
        trans.wallet_id = post.walletId
        trans.accountName = post.author.accountName
        trans.description = "در انتظار بررسی"
        trans.side = -1
        trans.save()

        # /////////// end save Transaction //////////////

        # /////////// save Assets //////////////

        assetsList = UserAssets.objects.filter(
            symbol=post.symbol).filter(author=request.user).values()

        if assetsList.count() <= 0:
            assets = UserAssets()
            assets.amount = post.amount
            assets.symbol = post.symbol
            assets.author = request.user
        else:
            assets = assetsList[0]
            assets.amount += post.amount

        assets.lastChange = timezone.now()
        assets.save()
        # /////////// end save Assets //////////////
    return render(request, "index.html", {'name': frm.errors, 'total': request.user.totalRial, 'block_': request.user.blockedRial})


@login_required
def index(request):
    # telegram
    # import threading
    # t = threading.Thread(target=background_process, args=(), kwargs={})
    # t.setDaemon(True)
    # t.start()
    return render(request, "index.html", {'name': request.user.username, 'total': request.user.totalRial, 'block_': request.user.blockedRial, "available": request.user.availableRial})


def CryptoRequest(request):
    MyLoginForm = CryptoBuyForm(request.POST)
    if MyLoginForm.is_valid():
        username = MyLoginForm.cleaned_data['username']


@login_required
def verification(request):
    if request.method == "GET":
        return render(request, "verification.html", {'unverifiedCardCount': request.user.unverifiedCardCount, 'verifiedLevel': request.user.verifiedLevel, 'name': request.user.username, 'total': request.user.totalRial, 'block_': request.user.blockedRial, "available": request.user.availableRial})
    try:
        if request.method == "POST":
            form = Verification(request.POST, request.FILES)
            author = request.user
            author.accountName = form.data['accountName']
            author.shabaNumber = form.data['shabaNumber']
            author.cardNumber = form.data['cardNumber']
            author.nationalCode = form.data['nationalCode']
            author.selfiImage = form.files['selfiImage']
            author.meliImage = form.files['meliImage']
            author.verifiedLevel = 0
            author.save()

            return render(request, "verification.html", {'unverifiedCardCount': request.user.unverifiedCardCount, 'verifiedLevel': request.user.verifiedLevel, 'name': request.user.username, 'total': request.user.totalRial, 'block_': request.user.blockedRial, "available": request.user.availableRial})
    except Exception:
        return render(request, "verification.html", {'unverifiedCardCount': request.user.unverifiedCardCount, 'verifiedLevel': request.user.verifiedLevel, 'name': request.user.username, 'total': request.user.totalRial, 'block_': request.user.blockedRial, "available": request.user.availableRial})


@login_required
def userCard(request):
    try:
        if request.method == "POST":
            form = UserCardForm(request.POST, request.FILES)
            author = request.user
            model = UserCard()
            model.cardImage = form.files['cardImage']
            model.cardNumber = form.data['cardNumber']
            model.date = timezone.now()
            model.author = author
            model.verifiedLevel = 0
            model.save()

        jData = UserCard.objects.filter(author=request.user).values()
        return render(request, "userCard.html", {'data': list(jData), 'name': request.user.username, 'total': request.user.totalRial, 'block_': request.user.blockedRial, "available": request.user.availableRial})
    except Exception:
        return render(request, "userCard.html", {'name': request.user.username, 'total': request.user.totalRial, 'block_': request.user.blockedRial, "available": request.user.availableRial})


def veriftyPhoneNumber(request):
    if request.method == "GET":
        return render(request, 'registration/veriftyPhoneNumber.html')
    if request.method == "POST":
        otp = request.POST['otp']
        phoneNumber = request.POST['phoneNumber']
        usr = list(Account.objects.filter(phoneNumber=phoneNumber))[0]
        if usr.otp == otp :
            usr.phoneIsVerified = True
            usr.save()
    return render(request, 'registration/login.html')
            

def generateCode(n=5):
    range_start = 10**(n-1)
    range_end = (10**n)-1
    return randint(range_start, range_end)


def sendOtp(phoneNumber):
    try:
        code = generateCode()
        usr = list(Account.objects.filter(phoneNumber=phoneNumber))[0]
        usr.otp = code
        print('code: '+ code)
        usr.otpCreatedAt = timezone.now()
        usr.phoneIsVerified = False
        usr.save()
        payloadToken = {
            "UserApiKey": "fd75e90eb3e755fe5e15e90",
            "SecretKey": "ab@digx"
        }
        r1 = requests.post('https://RestfulSms.com/api/Token',
                          data=json.dumps(payloadToken), headers={"Content-Type": "application/json"})
        v1 = json2obj(r1.content)
        token = v1.TokenKey

        # payload = {
        #     "Messages": [str(code)], "MobileNumbers": [phoneNumber], "LineNumber": "", "SendDateTime": "", "CanContinueInCaseOfError": False
        # } 
        
        payload = {
            "Code": str(code), "MobileNumber": phoneNumber
        }
        
        r = requests.post('http://RestfulSms.com/api/VerificationCode', data=json.dumps(payload), headers={
                          "Content-Type": "application/json", "x-sms-ir-secure-token": token})
        v = json2obj(r.content)
        n=0
    except Exception as e:
        m = 0


def register(request):
    message = ""
    if request.method == 'GET':
        return render(request, 'registration/sign_up.html')
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        usrs = list(Account.objects.filter(phoneNumber=request.POST['phoneNumber']))
        if len(usrs) > 0:
            if usrs[0].phoneIsVerified != True:
                sendOtp(form.data['phoneNumber'])
                return render(request, 'registration/veriftyPhoneNumber.html', {'phoneNumber': form.data['phoneNumber'], 'message': message})
            else:
                return render(request, 'registration/login.html', {'message': message})
        else:
            if form.is_valid():
                form.save()
                sendOtp(form.data['phoneNumber'])
                return render(request, 'registration/veriftyPhoneNumber.html', {'phoneNumber': form.data['phoneNumber'], 'message': message})
            else:
                message = "اطلاعات وارد شده صحیح نمی باشد"
                return render(request, 'registration/sign_up.html', {'message': message})
    else:
        form = RegistrationForm()
        return render(request, 'registration/sign_up.html', {'message': message})


def home(request):
    url = "https://api.binance.com/api/v3/ticker/24hr"

    # It is a good practice not to hardcode the credentials. So ask the user to enter credentials at runtime
    myResponse = requests.get(url)
    # print (myResponse.status_code)

    # For successful API call, response code will be 200 (OK)
    if(myResponse.ok):
        # Loading the response data into a dict variable
        # json.loads takes in only binary or string variables so using content to fetch binary content
        # Loads (Load String) takes a Json file and converts into python data structure (dict or list, depending on JSON)
        jData = json.loads(myResponse.content)
    else:
        # If response code is not ok (200), print the resulting http error code with description
        myResponse.raise_for_status()

    return render(request, "home.html", {'name': jData})


def homeTable(request):

    url = "https://api.binance.com/api/v3/ticker/24hr"

    # It is a good practice not to hardcode the credentials. So ask the user to enter credentials at runtime
    myResponse = requests.get(url)
    # print (myResponse.status_code)

    # For successful API call, response code will be 200 (OK)
    if(myResponse.ok):
        # Loading the response data into a dict variable
        # json.loads takes in only binary or string variables so using content to fetch binary content
        # Loads (Load String) takes a Json file and converts into python data structure (dict or list, depending on JSON)
        jData = json.loads(myResponse.content)
    else:
        # If response code is not ok (200), print the resulting http error code with description
        myResponse.raise_for_status()

    return JsonResponse({'data': jData}, safe=False)


def getPrice(coin):
    coin = coin.upper()
    ttrPrice = TTRPrice.objects.latest('id')
    if coin == "USDT":
        return ttrPrice.sell_price
    jData = client.get_all_tickers()
    data = list(jData)
    mdata = data[0]
    for item in data:
        if item["symbol"] == coin+"USDT":
            mdata = item

    res = float(mdata["price"]) * ttrPrice.buy_price
    return res


def getCoins(request):
    cryptos = Crypto.objects.values()
    return JsonResponse(list(cryptos), safe=False)

def getWallets(request, coin):
    cryptos = CryptoWallet.objects.filter(CryptoName=coin).values()
    return JsonResponse(list(cryptos), safe=False)


@login_required
def Withdraw(request, coin):
    if request.user.verifiedLevel != 1:
        return render(request, "verification.html", {'coin': coin, 'name': request.user.username, 'total': request.user.totalRial, 'block_': request.user.blockedRial, 'available': request.user.availableRial})
    if request.user.unverifiedCardCount > 0:
        return render(request, "userCard.html", {'coin': coin, 'name': request.user.username, 'total': request.user.totalRial, 'block_': request.user.blockedRial, 'available': request.user.availableRial})
    if(coin.lower() == "irr"):
        return render(request, "Withdrawirr.html", {'coin': coin, 'name': request.user.username, 'total': request.user.totalRial, 'block_': request.user.blockedRial, 'available': request.user.availableRial})
    return render(request, "Withdraw.html", {'coin': coin, 'name': request.user.username, 'total': request.user.totalRial, 'block_': request.user.blockedRial})


@login_required
def withdraw_crypto(request):
    message = ""
    if request.method == "GET":
        return render(request, "withdraw.html", {'total': request.user.totalRial, 'block_': request.user.blockedRial})
    form = CryptoWithdraw(request.POST)
    if form.is_valid():
        post = form.save(commit=False)
        if request.user.availableRial >= post.amount:
            post.author = request.user
            post.date = timezone.now()
            post.save()
            # /////////// save Transaction //////////////
            trans = TransAction()
            trans.amount = post.amount
            trans.author = post.author
            trans.symbol = post.symbol
            trans.network = post.network
            trans.order_date = post.date
            trans.last_action_date = timezone.now()
            trans.status = 0
            trans.wallet_id = post.walletId
            trans.tag = post.tag
            trans.description = "در انتظار بررسی"
            trans.side = -1
            trans.save()
        else:
            message = "insufficient balance"

        # /////////// end save Transaction //////////////

    return render(request, "withdraw.html", {'message': message, 'name': request.user.username, 'total': request.user.totalRial, 'block_': request.user.blockedRial, 'available': request.user.availableRial})


@login_required
def deposit(request, coin):
    if(coin.lower() == "irr"):
        return render(request, "depositirr.html", {'coin': coin, 'name': request.user.username, 'total': request.user.totalRial, 'block_': request.user.blockedRial})
    return render(request, "deposit.html", {'coin': coin, 'name': request.user.username, 'total': request.user.totalRial, 'block_': request.user.blockedRial})


def logout(reguest):
    from django.contrib.auth import logout
    from django.shortcuts import redirect
    logout(reguest)
    return redirect('home')


@login_required
def exchange(request):
    if request.method == "GET":
        return render(request, "exchange.html", {'name': request.user.username, 'total': request.user.totalRial, 'block_': request.user.blockedRial})
    else:
        message = ""
        try:
            if request.method == 'POST':
                mn = .09
                form = ExchangeForm(request.POST)
                post = form.save(commit=False)
                post.date = timezone.now()
                post.author = request.user
                post.date = timezone.now()
                post.save()
                assets = UserAssets.objects.filter(
                    author=request.user).filter(symbol=post.payCoin)
                p1 = getPrice(post.payCoin)
                p2 = getPrice(post.getCoin)
                getamount = p1 * post.paidAmount / p2
                if assets.count() > 0:
                    asset = assets[0]
                    asset.amount -= post.paidAmount
                    asset.lastChange = timezone.now()
                    asset.save()
                    uassets = UserAssets.objects.filter(
                        author=request.user).filter(symbol=post.getCoin)
                    if uassets.count() > 0:
                        uasset = uassets[0]
                        uasset.amount += getamount
                        uasset.lastChange = timezone.now()
                        uasset.save()
                    else:
                        uasset = UserAssets()
                        uasset.lastChange = timezone.now()
                        uasset.author = request.user
                        uasset.symbol = post.getCoin
                        uasset.amount = getamount
                        uasset.save()
                post.message = ""
                post.save()
        except Exception as e:

            post.message = e.message
            post.save()
            message = "متاسفانه خطایی رخ داد"
    return render(request, "exchange.html", {'name': request.user.username, 'total': request.user.totalRial, 'block_': request.user.blockedRial, 'message': message})


def background_process():
    import time
    while True:
        cryptosells = TransAction.objects.filter(amount=0).filter(side=-1)
        for sell in cryptosells:
            trans = list(client.get_deposit_history().values())
            for tran in trans[1]:
                if sell.txid == tran['txId']:
                    sell.txid = tran['txId']
                    sell.amount = tran['amount']
                    sell.symbol = tran['asset']
                    sell.statusb = tran['status']
                    sell.timeb = tran['insertTime']
                    sell.wallet_idb = tran['address']
                    sell.price = getPrice(sell.symbol)
                    sell.complet_date = timezone.now()
                    sell.description = "تکمیل شده"
                    sell.status = 2
                    sell.save()
                    if sell.statusb == 1:
                        uassets = UserAssets.objects.filter(author=request.user).filter(
                            symbol=sell.symbol).values()
                        if uassets.count() > 0:
                            uasset = uassets[0]
                            uasset += sell.amount
                            uasset.lastChange = timezone.now()
                            uasset.save()
                        else:
                            uasset = UserAssets()
                            uasset.lastChange = timezone.now()
                            uasset.author = sell.author
                            uasset.symbol = sell.symbol
                            uasset.amount = sell.amount
                            uasset.save()

    print("process started")
    time.sleep(100)
    print("process finished")


def default(obj):
    """Default JSON serializer."""
    import calendar
    import datetime

    if isinstance(obj, datetime.datetime):
        if obj.utcoffset() is not None:
            return str(obj)
    raise TypeError('Not sure how to serialize %s' % (obj,))
