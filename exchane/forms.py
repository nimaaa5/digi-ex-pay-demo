from django import forms

from .models import UserCard,Account,CryptoWithdraw , CryptoBuy, RialDeposit, CryptoSell, RialWithdraw , Account , Exchange


from django.contrib.auth.forms import UserCreationForm, UserChangeForm


class RegistrationForm(UserCreationForm):
    phoneNumber = forms.CharField(max_length=12,required=True)

    class Meta:
        model = Account
        fields = (
            'username',
            'phoneNumber',
            'password1',
            'password2'
        )

    def save(self, commit=True):
        user = super(RegistrationForm, self).save(commit=False)
        user.phoneNumber = self.cleaned_data['phoneNumber']

        if commit:
            user.save()

        return user

class ExchangeForm(forms.ModelForm):
    class Meta:
        model = Exchange
        fields = ('payCoin', 'getCoin', 'getAmount' , 'paidAmount' , )


class CompleteAccountForm(forms.ModelForm):
    class Meta:
        model = Account
        fields = ('shabaNumber', 'accountName', 'nationalCode' , )


class CryptoBuyForm(forms.ModelForm):
    class Meta:
        model = CryptoBuy
        fields = ('symbol', 'amount', 'walletId' , )


class CryptoWithdraw(forms.ModelForm):
    class Meta:
        model = CryptoWithdraw
        fields = ('symbol', 'network', 'walletId' , 'tag' , 'amount' , )


class RialDepositForm(forms.ModelForm):
    class Meta:
        model = RialDeposit
        fields = ('amount',)


class RialWithdrawForm(forms.ModelForm):
    class Meta:
        model = RialWithdraw
        fields = ('amount',)


class CryptoSellForm(forms.ModelForm):
    class Meta:
        model = CryptoSell
        fields = ('txId',)

class Verification(forms.ModelForm):
    class Meta:
        model=Account
        fields = ('shabaNumber','meliImage','selfiImage','nationalCode','accountName','cardNumber',)

class UserCardForm(forms.ModelForm):
    class Meta:
        model=UserCard
        fields = ('cardImage','cardNumber',)
