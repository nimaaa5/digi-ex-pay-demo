from django.db import models
from django.urls import reverse
from django.db import models
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager


class MyAccountManager(BaseUserManager):
    def create_user(self, email, username, password=None):
        if not email:
            raise ValueError('Users must have an email address')
        if not username:
            raise ValueError('Users must have a username')

        user = self.model(
            email=self.normalize_email(email),
            username=username,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, username, password):
        user = self.create_user(
            email=self.normalize_email(email),
            password=password,
            username=username,
        )
        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class Account(AbstractBaseUser):
    username = models.CharField(max_length=30, unique=True)
    date_joined = models.DateTimeField(
        verbose_name='date joined', auto_now_add=True)
    last_login = models.DateTimeField(verbose_name='last login', auto_now=True)
    is_admin = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    verifiedLevel = models.IntegerField(default=0,null=False)
    unverifiedCardCount = models.IntegerField(default=-1,null=False)
    totalRial = models.IntegerField(default=False)
    blockedRial = models.IntegerField(default=False)
    availableRial = models.IntegerField(default=False)
    phoneNumber = models.CharField(max_length=12, unique=True,default="")
    shabaNumber = models.CharField(max_length=30, null=True)
    otp = models.CharField(max_length=5, null=True)
    otpCreatedAt = models.DateField(auto_now_add=True, null=True)
    phoneIsVerified = models.BooleanField(default=False, null=True)
    accountName = models.CharField(max_length=30, null=True)
    cardNumber = models.CharField(max_length=30, null=True)
    nationalCode = models.CharField(max_length=30, null=True)
    meliImage = models.ImageField(upload_to = 'pic_folder/', default = 'pic_folder/None/no-img.jpg')
    selfiImage = models.ImageField(upload_to = 'pic_folder/', default = 'pic_folder/None/no-img.jpg')

    USERNAME_FIELD = 'phoneNumber'

    objects = MyAccountManager()

    def __str__(self):
        return self.phoneNumber

    def has_perm(self, perm, obj=None):
        return self.is_admin

    def has_module_perms(self, app_label):
        return True


class Exchange(models.Model):
    message = models.CharField(max_length=100, null=True)
    payCoin = models.CharField(max_length=100, null=False)
    getCoin = models.CharField(null=False, max_length=255)
    getAmount = models.FloatField(null=False)
    paidAmount = models.FloatField(null=False)
    author =  models.ForeignKey(Account, on_delete=models.CASCADE,null=True)
    date = models.DateField(null=True)

class CryptoWallet(models.Model):
    CryptoName = models.CharField(max_length=100, null=False)
    address = models.CharField(null=True, max_length=255)
    tag = models.CharField(null=True, max_length=255)
    network = models.CharField(null=True, max_length=255)


class Crypto(models.Model):
    symbol = models.CharField(max_length=100, null=False)
    minVolume = models.FloatField(default=0, null=False)
    name = models.CharField(null=True, max_length=255)


class CryptoBuy(models.Model):
    symbol = models.CharField(max_length=100, null=False)
    walletId = models.CharField(null=True, max_length=255)
    amount = models.FloatField(null=False)
    price = models.FloatField(null=True)
    author = models.ForeignKey(Account, on_delete=models.CASCADE)
    date = models.DateTimeField(null=False)

class UserCard(models.Model):
    cardImage = models.ImageField(upload_to = 'pic_folder/', default = 'pic_folder/None/no-img.jpg')
    cardNumber = models.CharField(max_length=100, null=False)
    author = models.ForeignKey(Account, on_delete=models.CASCADE)
    verifiedLevel = models.IntegerField(null=False, default=0)
    date = models.DateTimeField(null=False)


class CryptoWithdraw(models.Model):
    symbol = models.CharField(max_length=100, null=False)
    walletId = models.CharField(null=True, max_length=255)
    amount = models.FloatField(null=False)
    tag = models.CharField(max_length=100,null=True)
    network = models.CharField(max_length=100,null=True)
    author = models.ForeignKey(Account, on_delete=models.CASCADE)
    date = models.DateTimeField(null=False)


class CryptoSell(models.Model):
    symbol = models.CharField(max_length=100, null=True)
    amount = models.FloatField(null=True)
    price = models.FloatField(null=True)
    txId = models.CharField(max_length=100, null=False,unique=True)
    author = models.ForeignKey(Account, on_delete=models.CASCADE)
    date = models.DateTimeField(null=False)


class RialWithdraw(models.Model):
    amount = models.IntegerField(null=True)
    author = models.ForeignKey(Account, on_delete=models.CASCADE)
    date = models.DateTimeField(null=False)


class RialDeposit(models.Model):
    amount = models.IntegerField(null=True)
    author = models.ForeignKey(Account, on_delete=models.CASCADE)
    date = models.DateTimeField(null=False)


class TransAction(models.Model):
    amount = models.FloatField(null=False)
    author = models.ForeignKey(Account, on_delete=models.CASCADE)
    price = models.FloatField(null=True)
    symbol = models.CharField(null=False, max_length=10)
    order_date = models.DateTimeField(null=False)
    last_action_date = models.DateTimeField(null=False)
    wallet_id = models.CharField(null=True, max_length=100)
    wallet_idb = models.CharField(null=True, max_length=100) # b = binance
    tagAddress = models.CharField(null=True, max_length=100)
    txid = models.CharField(null=False, max_length=100)
    timeb = models.DateTimeField(null=True, max_length=100)
    complet_date = models.DateTimeField(null=True)
    description = models.CharField(null=True, max_length=255)
    status = models.IntegerField(null=False)
    statusb = models.IntegerField(null=True)
    accountName = models.CharField(null=True, max_length=255)
    side = models.IntegerField(null=False, default=-1)  # -1 = sell 1 = buy 


class vandarRequests(models.Model):
    amount = models.IntegerField(null=False)
    token = models.CharField(max_length=255, null=True)
    error = models.CharField(max_length=255, null=True)
    status = models.IntegerField(null=False)
    resstatus = models.IntegerField(null=True),
    resamount = models.CharField(max_length=255, null=True),
    resrealAmount = models.IntegerField(null=True),
    reswage = models.CharField(max_length=255, null=True),
    restransId = models.IntegerField(null=True),
    resfactorNumber = models.CharField(max_length=255, null=True),
    resmobile = models.CharField(max_length=255, null=True),
    resdescription = models.CharField(max_length=255, null=True),
    rescardNumber = models.CharField(max_length=255, null=True),
    respaymentDate = models.CharField(max_length=255, null=True),
    rescid = models.CharField(max_length=255, null=True),
    reserrors = models.CharField(max_length=255,null=True),
    resmessage = models.CharField(max_length=255, null=True),
    transAction = models.ForeignKey(TransAction , on_delete=models.CASCADE)


class UserAssets(models.Model):
    author = models.ForeignKey(Account, on_delete=models.CASCADE)
    symbol = models.CharField(max_length=10, null=False)
    lastChange = models.DateTimeField(null=False)
    amount = models.FloatField(null=False)


class TTRPrice(models.Model):
    date = models.DateTimeField(null=False)
    sell_price = models.FloatField(null=False)
    buy_price = models.FloatField(null=False, default=0)
